with Ada.Text_IO; use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;

with Measures; use Measures;
with Heart;
with HRM;
with ICD;
with ImpulseGenerator;

package body ClosedLoop is

   -- The initial value for the upper tach bound
   InitUpperTach : constant Measures.BPM := 100;

   -- The initial value for the joules to administer for vent fib
   InitJoulesForVentFib : constant Measures.Joules := 30;

   -- Time between ticks...
   TimeBetweenTicks : constant Duration := 0.1;

   -- Private function
   -- Returns a boolean indicating whether the authorised user is a cardiologist
   --  or clinical assistant and is connected to the patient of the device
   function IsAuthStaff(ICDSystem : in out ClosedLoopType) return Boolean is
      AuthUserPtr : User_Access;
   begin
      return FindUser(ICDSystem,
                      User_Strings.To_String(ICDSystem.AuthUser),
                      AuthUserPtr)
             and then
               AuthUserPtr.Role /= Patient
             and then
               HaveConn(ICDSystem,
                        User_Strings.To_String(ICDSystem.DeviceOwner),
                        User_Strings.To_String(ICDSystem.AuthUser));
   end IsAuthStaff;

   -- Private function
   -- Returns a boolean indicating whether the authorised user is a cardiologist
   --  and is connected to the patient of the device
   function IsAuthCardio(ICDSystem : in out ClosedLoopType) return Boolean is
      CardioUserPtr : User_Access;
   begin
      return FindUser(ICDSystem,
                      User_Strings.To_String(ICDSystem.AuthUser),
                      CardioUserPtr) 
             and then
               CardioUserPtr.Role = Cardiologist
             and then
               HaveConn(ICDSystem,
                        User_Strings.To_String(ICDSystem.DeviceOwner),
                        User_Strings.To_String(ICDSystem.AuthUser));
   end IsAuthCardio;


   -- Private function
   -- Returns a boolean indicating whether the authorised user is the owner of
   --  the device (patient) or if they are connected to the owner
   function IsAuthUser(ICDSystem : in out ClosedLoopType) return Boolean is
      UserPtr : User_Access;
   begin
      if FindUser(ICDSystem,
                  User_Strings.To_String(ICDSystem.AuthUser),
                  UserPtr) then
         if UserPtr.Username = ICDSystem.DeviceOwner then
            return true;
         elsif HaveConn(ICDSystem,
                          User_Strings.To_String(ICDSystem.DeviceOwner),
                          User_Strings.To_String(ICDSystem.AuthUser)) then
            return true;
         else
            return false;
         end if;
      else
         return false;
      end if;
   end IsAuthUser;

   -- Private procedure
   -- Update the array containing the last N impulses
   procedure UpdateImpulses(Impulses   : in out ImpulseArray;
                            Impulse    : in Joules;
                            Tick       : in Integer) is
      Temp     : ImpulseRecord;
      OldPrev  : ImpulseRecord;
   begin
      if Impulse > 0 then
         OldPrev.Impulse := Impulse;
         OldPrev.Tick := Tick;
         for I in Impulses'Range loop
            Temp := Impulses(I);
            Impulses(I) := OldPrev;
            OldPrev := Temp;
         end loop;
      end if;
   end UpdateImpulses;

   procedure Init(ICDSystem : out ClosedLoopType) is
   begin
      Heart.Init(ICDSystem.Hrt);
      HRM.Init(ICDSystem.Monitor);
      ImpulseGenerator.Init(ICDSystem.Generator);
      ICD.Init(ICDSystem.CardioDefib);
      ICDSystem.IsOn := False;
      ICDSystem.UpperTach := InitUpperTach;
      ICDSystem.JoulesForVentFib := InitJoulesForVentFib;
      ICDSystem.Ticks := 0;
      for I in ICDSystem.Impulses'Range loop
         ICDSystem.Impulses(I).Tick := -1;
      end loop;
   end Init;

   function On(ICDSystem : in out ClosedLoopType) return Boolean is
   begin
      if ICDSystem.IsOn = false and then IsAuthStaff(ICDSystem) then
         HRM.On(ICDSystem.Monitor, ICDSystem.Hrt);
         ImpulseGenerator.On(ICDSystem.Generator);
         ICDSystem.IsOn := True;
         return true;
      else
         return false;
      end if;
   end On;

   function Off(ICDSystem : in out ClosedLoopType) return Boolean is
   begin
      if ICDSystem.IsOn and then IsAuthStaff(ICDSystem) then
         HRM.Off(ICDSystem.Monitor);
         ImpulseGenerator.Off(ICDSystem.Generator);
         ICDSystem.IsOn := False;
         return true;
      else 
         return false;
      end if;
   end Off;

   function AddUser(ICDSystem  : in out ClosedLoopType;
                    Role       : in UserCategory;
                    Username   : in String)
                    return Boolean is
      NewUser : UserType;
   begin
      if ICDSystem.SystemUsers.FreeUserIndex = 
            ICDSystem.SystemUsers.Users'Last 
         or
            ICDSystem.IsOn then
         return false;
      else
         -- Create the new user
         NewUser.Role := Role;
         NewUser.Username := User_Strings.To_Bounded_String(Username);

         -- Add the new user and update the free index spot
         ICDSystem.SystemUsers.Users(ICDSystem.SystemUsers.FreeUserIndex) :=
            NewUser;
         ICDSystem.SystemUsers.FreeUserIndex :=
            ICDSystem.SystemUsers.FreeUserIndex + 1;
         return true;
      end if;
   end AddUser;

   function FindUser(ICDSystem   : in out ClosedLoopType;
                      Username   : in String;
                      UserPtr    : out User_Access)
                      return Boolean is
   begin
      for I in ICDSystem.SystemUsers.Users'Range loop
         if ICDSystem.SystemUsers.Users(I).Username =
               User_Strings.To_Bounded_String(Username) then
            UserPtr := ICDSystem.SystemUsers.Users(I)'Unrestricted_Access;
            return true;
         end if;
      end loop;
      return false;
   end FindUser;

   function AddConn(ICDSystem : in out ClosedLoopType;
                    Username1 : in String;
                    Username2 : in String)
                    return Boolean is
      User1Ptr : User_Access;
      User2Ptr : User_Access;
   begin
      if ICDSystem.IsOn = false and then
         FindUser(ICDSystem, Username1, User1Ptr) and then
         FindUser(ICDSystem, Username2, User2Ptr) then

         -- Add connection to user 1 and update free index
         User1Ptr.Conns(User1Ptr.FreeConnIndex) :=
            User_Strings.To_Bounded_String(Username2);
         User1Ptr.FreeConnIndex := User1Ptr.FreeConnIndex + 1;

         -- Add connection to user 2 and update free index
         User2Ptr.Conns(User2Ptr.FreeConnIndex) :=
            User_Strings.To_Bounded_String(Username1);
         User2Ptr.FreeConnIndex := User2Ptr.FreeConnIndex + 1;

         return true;
      else
         return false;
      end if;
   end AddConn;

   function HaveConn(ICDSystem : in out ClosedLoopType;
                     Username1 : in String;
                     Username2 : in String)
                     return Boolean is
      User1Ptr : User_Access;
      User2Ptr : User_Access;
   begin
      if FindUser(ICDSystem, Username1, User1Ptr) and then
            FindUser(ICDSystem, Username2, User2Ptr) then

         -- loop through user connections and return true if user 2 found
         for I in User1Ptr.Conns'Range loop
            if User1Ptr.Conns(I) = User2Ptr.Username then
               return true;
            end if;
         end loop;

      end if;
      return false;
   end HaveConn;

   function SetPatient(ICDSystem : in out ClosedLoopType;
                       Username  : in String)
                       return Boolean is
      UserPtr : User_Access;
   begin
      if ICDSystem.IsOn = false and then
            FindUser(ICDSystem, Username, UserPtr) and then
            UserPtr.Role = Patient then
         ICDSystem.DeviceOwner := UserPtr.Username;
         return true;
      else
         return false;
      end if;
   end SetPatient;

   function AuthenticateUser(ICDSystem : in out ClosedLoopType;
                             Username  : in String)
                             return Boolean is
      UserPtr : User_Access;
   begin
      if ICDSystem.IsOn = false and then
            FindUser(ICDSystem, Username, UserPtr) then
         ICDSystem.AuthUser := UserPtr.Username;
         return true;
      else
         return false;
      end if;
   end AuthenticateUser;

   function ReadSettings(ICDSystem           : in out ClosedLoopType;
                         UpperTach           : out BPM;
                         JoulesForVentFib    : out Joules)
                         return Boolean is
   begin
      if ICDSystem.IsOn = false and then IsAuthStaff(ICDSystem) then
         UpperTach := ICDSystem.UpperTach;
         JoulesForVentFib := ICDSystem.JoulesForVentFib;
         return true;
      else
         return false;
      end if;
   end ReadSettings;

   function SetUpperTach(ICDSystem : in out ClosedLoopType;
                         UpperTach : in BPM)
                         return Boolean is
   begin
      if ICDSystem.IsOn = false and then IsAuthCardio(ICDSystem) then
         ICDSystem.UpperTach := UpperTach;
         return true;
      else
         return false;
      end if;
   end SetUpperTach;

   function SetJoulesForVentFib(ICDSystem          : in out ClosedLoopType;
                                JoulesForVentFib   : in Joules)
                                return Boolean is
   begin
      if ICDSystem.IsOn = false and then IsAuthCardio(ICDSystem) then
         ICDSystem.JoulesForVentFib := JoulesForVentFib;
         return true;
      else
         return false;
      end if;
   end SetJoulesForVentFib;

   function ReadImpulses(ICDSystem  : in out ClosedLoopType;
                         Impulses   : out ImpulseArray)
                         return Boolean is
   begin
      if ICDSystem.IsOn and then IsAuthUser(ICDSystem) then
         Impulses := ICDSystem.Impulses;
         return true;
      else
         return false;
      end if;
   end ReadImpulses;

   procedure Tick(ICDSystem : in out ClosedLoopType) is
      Impulse : Joules;
   begin
      if ICDSystem.IsOn then
         -- Get the current heart rate
         HRM.GetRate(ICDSystem.Monitor, ICDSystem.HeartRate);

         -- Output the current heart rate
         Put("After turn on = ");
         Put(Item => ICDSystem.HeartRate);
         New_Line; 

         -- Calc the next impulse, set the impulse in the generator and update
         --  the record of the last N impulses
         Impulse := ICD.CalcImpulse(ICDSystem.HeartRate,
                                    ICDSystem.CardioDefib,
                                    TimeBetweenTicks,
                                    ICDSystem.UpperTach,
                                    ICDSystem.JoulesForVentFib);
         ImpulseGenerator.SetImpulse(ICDSystem.Generator, Impulse);
         UpdateImpulses(ICDSystem.Impulses, Impulse, ICDSystem.Ticks);

         -- Tick the components in the ICD System
         ImpulseGenerator.Tick(ICDSystem.Generator, ICDSystem.Hrt);
         HRM.Tick(ICDSystem.Monitor, ICDSystem.Hrt);
         Heart.Tick(ICDSystem.Hrt);

         -- Update the number of ticks
         ICDSystem.Ticks := ICDSystem.Ticks + 1;

         -- wait to mimic the time between ticks
         delay TimeBetweenTicks;
      end if;
   end Tick;

end ClosedLoop;