with Ada.Strings.Bounded;

with Measures; use Measures;
with Heart;
with HRM;
with ICD;
with ImpulseGenerator;

-- This package encapsulates to entire ICD system. It exposes functionality
--  for using the system as a whole, including 'ticking' the system, reading
--  and updating settings, managing users and authentication and turning the
--  system off and on.
package ClosedLoop is

   -- Max length of a username
   MaxUsername : constant Integer := 20;

   -- Max number of connections a user can have
   MaxConns : constant Integer := 10;

   -- Max number of users the system can contain
   MaxUsers : constant Integer := 100;

   -- The number of previous impulses to store
   NLastImpulses : constant Integer := 50;

   -- Bounded string to be used for usernames
   package User_Strings is new 
      Ada.Strings.Bounded.Generic_Bounded_Length (Max => MaxUsername);
   use User_Strings;

   -- Enum of possible categories of users
   type UserCategory is (Patient, Cardiologist, ClinicalAssistant);

   -- Type for the range of connections
   subtype ConnsIndex is Integer range 1 .. MaxConns;

   -- Type used to store connections, an array of usernames
   type ConnsArray is array(ConnsIndex) of User_Strings.Bounded_String;

   -- Type used to represent a User
   type UserType is
      record
         Role           : UserCategory;
         Username       : User_Strings.Bounded_String;
         Conns          : ConnsArray;
         FreeConnIndex  : ConnsIndex := ConnsIndex'First;
      end record;

   -- Type for pointers to a User
   type User_Access is access all UserType;

   -- Type for the range of users
   subtype UsersIndex is Integer range 1 .. MaxUsers;

   -- Type used to store users, an array of users
   type UsersArray is array(UsersIndex) of aliased UserType;

   -- Type used to store Users and indicate the free index
   type UsersType is
      record
         Users          : UsersArray; 
         FreeUserIndex  : UsersIndex := UsersIndex'First;
      end record;

   -- Type to record an impulse and its tick
   type ImpulseRecord is
      record
         Impulse  : Joules;
         Tick     : Integer;
      end record;

   -- Type to store last N impulses and their ticks
   type ImpulseArray is array(Integer range 1..NLastImpulses) of ImpulseRecord;

   -- Type used to encapsulate the whole ICD System
   type ClosedLoopType is
      record
         Hrt               : Heart.HeartType;
         Monitor           : HRM.HRMType;
         Generator         : ImpulseGenerator.GeneratorType;
         HeartRate         : BPM;
         CardioDefib       : ICD.ICDType;
         SystemUsers       : UsersType;
         DeviceOwner       : User_Strings.Bounded_String;
         AuthUser          : User_Strings.Bounded_String;
         IsOn              : Boolean;
         UpperTach         : BPM;
         JoulesForVentFib  : Joules;
         Ticks             : Integer;
         Impulses          : ImpulseArray;
      end record;

   -- Initialise the ICD System
   procedure Init(ICDSystem : out ClosedLoopType);

   -- Turns on the ICD System
   --  Returns a boolean representing whether the function was successful
   function On(ICDSystem : in out ClosedLoopType) return Boolean;

   -- Turns off the ICD System
   --  Returns a boolean representing whether the function was successful
   function Off(ICDSystem : in out ClosedLoopType) return Boolean;

   -- Adds a user to the system
   --  Returns a boolean representing whether the function was successful
   function AddUser(ICDSystem : in out ClosedLoopType;
                     Role     : in UserCategory;
                     Username : in String)
                     return Boolean;

   -- Finds a user within the system, providing a pointer to them
   --  Returns a boolean representing whether the function was successful
   function FindUser(ICDSystem   : in out ClosedLoopType;
                      Username   : in String;
                      UserPtr    : out User_Access)
                      return Boolean;

   -- Adds a connection between two users
   --  Returns a boolean representing whether the function was successful
   function AddConn(ICDSystem : in out ClosedLoopType;
                    Username1 : in String;
                    Username2 : in String)
                    return Boolean;

   -- Returns whether two users have a connection between them
   function HaveConn(ICDSystem   : in out ClosedLoopType;
                     Username1   : in String;
                     Username2   : in String)
                     return Boolean;

   -- Sets the owner/patient using the device
   --  Returns a boolean representing whether the function was successful
   function SetPatient(ICDSystem : in out ClosedLoopType;
                       Username  : in String)
                       return Boolean;

   -- Sets the authorised user
   --  Returns a boolean representing whether the function was successful
   function AuthenticateUser(ICDSystem : in out ClosedLoopType;
                             Username  : in String)
                             return Boolean;

   -- Read the settings and output the value for the upper tach bound and joules
   --  to apply for vent fib
   --  Returns a boolean representing whether the function was successful
   function ReadSettings(ICDSystem           : in out ClosedLoopType;
                         UpperTach           : out BPM;
                         JoulesForVentFib    : out Joules)
                         return Boolean;

   -- Sets the upper bound for tach treatment
   --  Returns a boolean representing whether the function was successful
   function SetUpperTach(ICDSystem : in out ClosedLoopType;
                         UpperTach : in BPM)
                         return Boolean;

   -- Set the joules to apply for vent fib
   --  Returns a boolean representing whether the function was successful
   function SetJoulesForVentFib(ICDSystem          : in out ClosedLoopType;
                                JoulesForVentFib   : in Joules)
                                return Boolean;

   -- Read the last N impulses recorded in the system
   --  Returns a boolean representing whether the function was successful
   function ReadImpulses(ICDSystem  : in out ClosedLoopType;
                         Impulses   : out ImpulseArray)
                         return Boolean;

   -- Tick the entire system 0.1 seconds
   procedure Tick(ICDSystem : in out ClosedLoopType);

end ClosedLoop;