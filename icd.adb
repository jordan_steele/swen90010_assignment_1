with Ada.Text_IO; use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;

with Measures;

package body ICD is

   -- How many BPM above the HR the Tach treatment should be applied
   TachRateAboveHR : constant Measures.BPM := 15;

   -- The average change over the previous N HR readings to cause vent fib
   AvgChgForVent : constant Integer := 10;

   -- Joules to apply for each impulse of a Tach treatment
   ImpulseForTach : constant Measures.Joules := 2;

   -- The Joules to apply for no treatment
   NoTreatImpulse : constant Measures.Joules := 0;

   -- A private procedure to reset the vent imp array
   procedure InitVentImpArray(Icd : in out ICDType) is
   begin
      for I in Icd.VentImps'Range loop
         Icd.VentImps(I) := -1;
      end loop;
   end InitVentImpArray;

   -- A private procedure to update the previous N HR readings stored with the
   --  latest HR reading
   procedure UpdateVentImps(Rate : in Measures.BPM; Icd : in out ICDType) is
      Temp     : Measures.BPM;
      OldPrev  : Measures.BPM := Rate;
   begin
      for I in Icd.VentImps'Range loop
         Temp := Icd.VentImps(I);
         Icd.VentImps(I) := OldPrev;
         OldPrev := Temp;
      end loop;
   end UpdateVentImps;

   -- A private function to determine whether a vent fib treatment should be
   --  applied
   --  Returns a boolean representing this
   function IsVentFib(Rate : in Measures.BPM;
                      Icd : in out ICDType)
                      return Boolean is
      TotalChange    : Integer := 0;
      TotalCounted   : Integer := 0;
      AverageChange  : Integer;
   begin
      -- Calculate the total change in stored HRs and how many we counted
      for I in Icd.VentImps'First .. Icd.VentImps'Last - 1 loop
         exit when Icd.VentImps(I+1) = -1;
         TotalCounted := TotalCounted + 1;
         TotalChange := TotalChange + abs(Icd.VentImps(I) - Icd.VentImps(I+1));
      end loop;

      -- If we've counted less then we need for vent fib, return false
      if TotalCounted < NHeartRForVent then
         return false;
      end if;

      -- Calc the average change and return true if larger than required amount
      AverageChange := TotalChange / TotalCounted;
      if AverageChange >= AvgChgForVent then
         -- reset vent imp array, otherwise we just keep throwing vent fibs
         InitVentImpArray(Icd);
         return true;
      else
         return false;
      end if;
   end IsVentFib;

   -- A private function to determine whether a tach treatment should be applied
   -- Returns a boolean representing this
   function IsTachycardia(Rate               : in Measures.BPM;
                          Icd                : in out ICDType;
                          TimeSinceLastImp   : in Duration;
                          UpperTach          : in Measures.BPM)
                          return Boolean is
   begin
      -- If we are currently in a tach treatment
      if Icd.TachImpsLeft > TachCountdown'First then
         Icd.TachNextImpIn := Icd.TachNextImpIn - TimeSinceLastImp;
         -- If it's time for another impulse
         if Icd.TachNextImpIn < 0.0 then
            -- update variables and return true
            Icd.TachNextImpIn := Icd.TachImpGap;
            Icd.TachImpsLeft := Icd.TachImpsLeft - 1;
            return true;
         else
            return false;
         end if;
      elsif Rate > UpperTach then
         Icd.TachImpsLeft := TachCountdown'Last;
         Icd.TachImpGap := Duration(60) / Duration(Rate + TachRateAboveHR);
         Icd.TachNextImpIn := Icd.TachImpGap;
         return true;
      else
         return false;
      end if;
   end IsTachycardia;

   procedure Init(Icd : out ICDType) is
   begin
      InitVentImpArray(Icd);
      -- for I in Icd.VentImps'Range loop
      --    Icd.VentImps(I) := -1;
      -- end loop;
      Icd.TachImpsLeft := TachCountdown'First;
   end Init;

   function CalcImpulse(Rate              : in Measures.BPM;
                        Icd               : out ICDType;
                        TimeSinceLastImp     : in Duration;
                        UpperTach         : in Measures.BPM;
                        JoulesForVentFib  : in Measures.Joules)
                        return Measures.Joules is
   begin
      UpdateVentImps(Rate, Icd);

      if IsVentFib(Rate, Icd) then
         return JoulesForVentFib;
      elsif IsTachycardia(Rate, Icd, TimeSinceLastImp, JoulesForVentFib) then
         return ImpulseForTach;
      else
         return NoTreatImpulse;
      end if;
   end CalcImpulse;

end ICD;