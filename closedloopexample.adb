with Ada.Text_IO; use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;

with Measures; use Measures;
with ClosedLoop;

-- This procedure shows example invocations of our entire system
--  It first highlights the permissions of the system and then runs the system
--  for 300 ticks
--  I've found the system runs well with the following changes to the heart
--       - SmallShock := 45
--       - DefaultChange := 9
--  This should show vent fib and tach both being run!
procedure ClosedLoopExample is
   ICDSystem : ClosedLoop.ClosedLoopType; -- the entire system

   -- just some variables to store values we read from the system
   UpperTach : BPM;
   JoulesForVentFib : Joules;
   Impulses : ClosedLoop.ImpulseArray;

   procedure AddUsers is
   begin
      if ClosedLoop.AddUser(ICDSystem, ClosedLoop.Patient, "Jordan") then
         Put("Added a new Patient named Jordan");
         New_Line;
      else
         Put("Error adding new User");
         New_Line;
      end if;

      if ClosedLoop.AddUser(ICDSystem, ClosedLoop.Cardiologist, "Tim") then
         Put("Added a new Cardiologist named Tim");
         New_Line;
      else
         Put("Error adding new User");
         New_Line;
      end if;

      if ClosedLoop.AddUser(ICDSystem, ClosedLoop.ClinicalAssistant, "Qingyu") then
         Put("Added a new Clinical Assistant named Qingyu");
         New_Line;
      else
         Put("Error adding new user");
         New_Line;
      end if;
   end AddUsers;

   procedure SetPatient is
   begin
      if ClosedLoop.SetPatient(ICDSystem, "Jordan") then
         Put("Set Jordan as the Patient of this device");
         New_Line;
      else
         Put("Error setting the patient of the device");
         New_Line;
      end if;
   end SetPatient;

   procedure AddConns is
   begin
      if ClosedLoop.AddConn(ICDSystem, "Jordan", "Tim") then
         Put("Added a relationship between Jordan and Tim");
         New_Line;
      else
         Put("Error adding relationship");
         New_Line;
      end if;
      if ClosedLoop.AddConn(ICDSystem, "Jordan", "Qingyu") then
         Put("Added a relationship between Jordan and Qingyu");
         New_Line;
      else
         Put("Error adding relationship");
         New_Line;
      end if;
   end AddConns;

   procedure TestSettings is
   begin
      if ClosedLoop.ReadSettings(ICDSystem, UpperTach, JoulesForVentFib) then
         Put("Read Settings: ");
         Put(UpperTach);
         Put(JoulesForVentFib);
         New_Line;
      else
         Put("Error reading settings");
         New_Line;
      end if;

      if ClosedLoop.SetUpperTach(ICDSystem, 120) then
         Put("Set upper bound for tachycardia to 120");
         New_Line;
      else
         Put("Error settings upper bound for tachycardia");
         New_Line;
      end if;

      if ClosedLoop.SetJoulesForVentFib(ICDSystem, 44) then
         Put("Set upper bound for joules for vent fib to 20");
         New_Line;
      else
         Put("Error setting joules for vent fib");
         New_Line;
      end if;
   end TestSettings;

   procedure TryAuthenticate(Username : in String) is
   begin
      if ClosedLoop.AuthenticateUser(ICDSystem, Username) then
         Put("Authenticated as ");
         Put(Username);
         New_Line;
      else 
         Put("Error authenticating user");
         New_Line;
      end if;
   end TryAuthenticate;

   procedure TryOn is
   begin
      if ClosedLoop.On(ICDSystem) then
         Put("Turned device on!");
         New_Line;
      else
         Put("Error turning device on!");
         New_Line;
      end if;
   end TryOn;

   procedure TryOff is
   begin
      if ClosedLoop.Off(ICDSystem) then
         Put("Turned device off!");
         New_Line;
      else
         Put("Error turning device off!");
         New_Line;
      end if;
   end TryOff;

   procedure ReadAndPrintImpulses is
   begin
      Put("Reading Last 50 Impulses");
      New_Line;
      if ClosedLoop.ReadImpulses(ICDSystem, Impulses) then
         for I in Impulses'Range loop
            exit when Impulses(I).Tick = -1;
            Put(Impulses(I).Tick);
            Put(Impulses(I).Impulse);
            New_Line;
         end loop;
      else
         Put("Error whilst reading impulses");
         New_Line;
      end if;
   end ReadAndPrintImpulses;

begin
   ClosedLoop.Init(ICDSystem);

   AddUsers;
   SetPatient;
   TestSettings;
   ReadAndPrintImpulses;

   AddConns;
   TestSettings;
   ReadAndPrintImpulses;

   TryAuthenticate("Jordan");
   TestSettings;
   ReadAndPrintImpulses;

   TryAuthenticate("Qingyu");
   TestSettings;
   ReadAndPrintImpulses;

   TryAuthenticate("Tim");
   TestSettings;
   ReadAndPrintImpulses;

   TryOn;

   TestSettings;
   ReadAndPrintImpulses;

   for I in Integer range 0..300 loop
      ClosedLoop.Tick(ICDSystem);
   end loop;

   ReadAndPrintImpulses;

   TryOff;

end ClosedLoopExample;