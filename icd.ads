with Measures;

-- This package simulates the software for a implantable cardioverter-
--  defibrillator. It is provided with a heart rate and responds with the
--  correct amount of Joules to discharge.
package ICD is

   -- How many of the previous HR readings to calc vent fib
   NHeartRForVent : constant Integer := 6;

   -- How many impulses to apply for a tach treatment
   NTachSignals : constant Integer := 10;

   -- Type for the range of HR readings to store to calc vent fib
   subtype VentImpIndex is Integer range 1 .. NHeartRForVent + 1;

   -- A type to hold the last N HR readings for calculating vent fib
   type VentImpArray is
      array(VentImpIndex) of Measures.BPM;

   -- A type to be used when counting down the tach impulses
   type TachCountdown is range 1 .. NTachSignals;

   -- A type representing the ICD software to store varibles for calculations
   type ICDType is
      record
         VentImps : VentImpArray;      -- Holds imps for vent fib
         TachImpsLeft : TachCountdown; -- Remaining imps for tach treatment
         TachImpGap: Duration;         -- Gap between imp in this tach treatment
         TachNextImpIn : Duration;     -- Time till next impulse for tach
      end record;

   -- Initialise the ICD software
   procedure Init(Icd : out IcdType);

   -- Takes the current rate and data needed for calculations and returns
   --  the amount of Joules to apply
   function CalcImpulse(Rate              : in Measures.BPM;
                        Icd               : out ICDType;
                        TimeSinceLastImp  : in Duration;
                        UpperTach         : in Measures.BPM;      
                        JoulesForVentFib  : in Measures.Joules)
                        return Measures.Joules;

end ICD;